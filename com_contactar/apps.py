from django.apps import AppConfig


class ComContactarConfig(AppConfig):
    name = 'com_contactar'
