from django.db import models

from wagtail.wagtailcore.models import Page
from wagtail.wagtailcore.fields import RichTextField


class CarouselItem(Page):

    body = RichTextField()
    buttonText = models.CharField(max_length=20)
    buttonUrl = models.URLField()