from __future__ import absolute_import, unicode_literals

from django.db import models

from wagtail.wagtailcore.models import Page
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import FieldPanel


class HomePage(Page):
    body = RichTextField(blank=True)

    content_panels = Page.content_panels + [
            FieldPanel('body', classname="full"),
    ]

class HomeSnippetList(Page):
    intro = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full"),
    ]

class HomeSnippet(Page):
    body = RichTextField('url', blank=True)
    #url = models.URLField()

    content_panels = Page.content_panels + [
            #ImageChooserPanel('url'),
            FieldPanel('body', classname="full"),
    ]
