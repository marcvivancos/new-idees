from django.apps import AppConfig


class QuiSomConfig(AppConfig):
    name = 'qui_som'
